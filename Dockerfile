# imagen a utiliza
FROM ubuntu

# ejecuta comando de instalacion apache
RUN apt update && apt install apache2 -y

# Abre puertos del contenedor
EXPOSE 80 443

# ejecuta comando para que se ejecute apache dentro del contenedor
CMD ["apache2ctl", "-D", "FOREGROUND"
