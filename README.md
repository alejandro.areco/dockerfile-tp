# Dockerfile - TP 2

**Nombre y Apellido:** Alejandro Areco

**Materia:** Infraestructura de Servidores

**Profesor:** Sergio Pernas


## Instalacion Docker

**Como primer punto, instalo docker en la VM**

"root@ubuntu:/home/alejandro# apt install docker.io"

## Creacion imagen Docker con Ubuntu
root@ubuntu:/home/alejandro# docker run -ti \
--name server \
-p 8000:80 \
-v $HOME/tmp:/mnt \
ubuntu

**Dentro de la imagen, instalo apache2**

root@03b49e5e3e0f:/# apt update && apt install apache2 -y

**El acceso a apache no funciona, porque los contenederos no tienen servicios por lo cual ejecuto:**

apache2ctl -D FOREGROUND

**Al ejecutar el comando anterior, aparece el siguiente error:**

*AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.17.0.2. Set the 'ServerName' directive globally to suppress this message*

Para suprimir este error, dentro del archivo de configuracion de apache en el contenedor añado 'ServerName localhost' al final.

root@03b49e5e3e0f:/# vim /etc/apache2/apache2.conf

**Reinicio apache para aplicar los cambios**

```
root@03b49e5e3e0f:/# service apache2 restart
 * Restarting Apache httpd web server apache2                                                 [ OK ]
```

Pruebo el acceso desde mi pc local http://192.168.0.240:8000/ , lo que muestra la pagina de bienvenida de Apache correctamente.

## Creacion Dockerfile

**Creo directorio donde alojare el dockerfile**

root@ubuntu:/home/alejandro# mkdir tp-2

root@ubuntu:/home/alejandro/tp-2# vim Dockerfile

root@ubuntu:/home/alejandro/tp-2# cat Dockerfile
```
# imagen a utilizar
FROM ubuntu

# ejecuta comando de instalacion apache
RUN apt update && apt install apache2 -y

# Abre puertos del contenedor
EXPOSE 80 443

# ejecuta comando para que se ejecute apache dentro del contenedor
CMD ["apache2ctl", "-D", "FOREGROUND"]
```

## Creacion Imagen Docker

root@ubuntu:/home/alejandro/tp-2# docker build -t apache-image-tp2 .

```
DEPRECATED: The legacy builder is deprecated and will be removed in a future release.
            Install the buildx component to build images with BuildKit:
            https://docs.docker.com/go/buildx/

Sending build context to Docker daemon  2.048kB
Step 1/4 : FROM ubuntu
 ---> 17c0145030df
Step 2/4 : RUN apt update && apt install apache2 -y
 ---> Using cache
 ---> ff138f1b4480
Step 3/4 : EXPOSE 80 443
 ---> Running in 10bc9629a6f5
Removing intermediate container 10bc9629a6f5
 ---> 62902d93177e
Step 4/4 : CMD ["apache2ctl", "-D", "FOREGROUND"
 ---> Running in bee415f52c47
Removing intermediate container bee415f52c47
 ---> 8a8ebaa98087
Successfully built 8a8ebaa98087
Successfully tagged apache-image-tp2:latest
```

**Verifico correctan creacion de imagen**
```
root@ubuntu:/home/alejandro/tp-2# docker images
REPOSITORY         TAG       IMAGE ID       CREATED          SIZE
apache-image-tp2   latest    8a8ebaa98087   26 seconds ago   222MB
ubuntu             latest    17c0145030df   2 weeks ago      76.2MB
```
